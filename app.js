const express = require('express')
const morgan = require('morgan')

const PORT = process.env.PORT || '3000'

const app = express()
let count = 0

app.use(morgan('tiny'))

app.get('/', (req, res) => res.send(`<h1>Hello world</h1><p>Count: ${count++}</p>`))

app.listen(PORT, () => console.log(`Server started on port ${PORT}...`))
